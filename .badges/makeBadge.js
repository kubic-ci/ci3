
function svgBadge(args) {

	const badge = require('gh-badges')

	// Optional step, to have accurate text width computation.
	const format = {
	  // ['build', 'passed']
	  text: args,
	  // See https://github.com/badges/shields/blob/master/lib/colorscheme.json
	  colorscheme: 'blue',
	  template: 'flat',
	}

	badge(format, (svg, err) => {
	    // console.log(err);
	    process.stdout.write(svg);
	})
}

// Cut first two words and use them as the text of the badge
var words = process.argv.slice(2,4);
svgBadge(words);
