#!/bin/bash
#
# This script will update static code badges used by `../README.md`
#
# Prerequisites: 
# 	npm install -g gh-badges 
#

# version badge
VERSION=`python -c "exec(open('../ci3/version.py').read()); print(__version__)"`
node makeBadge.js version $VERSION > version.svg

# license badge
node makeBadge.js license MIT > license.svg

# blog badge
node makeBadge.js blog Medium > blog.svg