"""Docker commands."""
import sys
import logging
from sh import docker, ErrorReturnCode

from .base import cd
from .dotci3 import ContainerCliCommand
from ci3.error import Ci3Error


logger = logging.getLogger(__name__)


def push_image(tag):
    """Push docker image via gcloud context to resolve permission issues."""
    docker('--', 'push', tag, _out=sys.stdout, _err=sys.stderr)


class BuildCommand(ContainerCliCommand):
    """Build container images with docker."""

    def run(self, args):
        """Call docker to build image."""
        self.load_vars()
        for name in self.get_selected_containers(args):
            values = self.config_vars['containers'][name]
            image_registry_url = self.config_vars['cluster']['image_registry_url']
            tag_tail = values['image'].get('tag', 'last')
            if (values['image']['tag'] == '@git-branch@'):
                # Tag with branch name. 
                tag_tail = self.git_branch_ending()
            elif (values['image']['tag'] == '@git-head-sha@'):
                tag_tail = 'commit-%s' % self.get_head_sha()
            tag = "{}/{}:{}".format(
                image_registry_url,
                values['image']['name'],
                tag_tail)
            try:
                logger.info('Building %s..' % name)
                logger.info('.. tagged as: %s' % tag)
                options = ['.']
                if 'build' in values and 'options' in values['build']:
                    options = values['build']['options'].split(" ") + options
                subpath = '.'
                if 'build' in values and 'subpath' in values['build']:
                    subpath += '/' + values['build']['subpath']
                with cd(subpath):
                    
                    docker.build('-t', tag, *options, _out=sys.stdout, _err=sys.stderr)
                logger.info('Done')
            except ErrorReturnCode as error:
                raise Ci3Error("Failed to build docker image `{}`: {}"
                               .format(name, error))


class PushCommand(ContainerCliCommand):
    """Push container images to docker registry."""

    def _push(self, tag):
        logger.info('Pushing %s..' % tag)
        if self.config_vars['cluster']['type'] == 'gke':
            # use docker push directly
            push_image(tag)
        else:
            docker.push(tag, _out=sys.stdout, _err=sys.stderr)
        logger.info('Done')

    def _tag_remote(self, exising_tag, new_tag):
        if self.config_vars['cluster']['type'] == 'gke':
            from .gke import tag_container
            logger.info('Adding tag %s..' % new_tag)
            tag_container(exising_tag, new_tag)
            logger.info('Done')

    def run(self, args):
        """Call docker to push image."""
        self.load_vars()
        for name in self.get_selected_containers(args):
            values = self.config_vars['containers'][name]
            image_registry_url = self.config_vars['cluster']['image_registry_url']
            # Tag with branch name.
            tag = "{}/{}:{}".format(
                image_registry_url,
                values['image']['name'],
                self.git_branch_ending())
            try:
                # Tag with git sha
                tag_sha = "{}/{}:{}".format(
                    image_registry_url,
                    values['image']['name'],
                    'commit-' + self.get_head_sha())
                docker.tag(tag, tag_sha, _out=sys.stdout, _err=sys.stderr)
                # .. and then push
                self._push(tag_sha)
                self._tag_remote(tag_sha, tag)
            except ErrorReturnCode as error:
                raise Ci3Error("Failed to push docker image `{}`: {}"
                               .format(tag, error))
